using System;
using System.Text;

public class FormaleSprache {
  public static void Main() {
    StringBuilder wort = new StringBuilder("S");
    string kette = wort + " -> ";
    int eingabe = -1;
    Console.WriteLine("Produktionsregeln: 1. X->aX, 2. X->a, 3. X->aY, 4. Y->aa, 5. S->X");
    Console.WriteLine("");  
    Console.WriteLine(kette + " ... ");    
    while(eingabe != 0){
        Console.WriteLine("Geben Sie eine Zahl zwischen 0 und 5 ein (0 = Ende), dann Enter:");
        eingabe = Int32.Parse(Console.ReadLine());
        if(eingabe == 1) {
            if(wort[wort.Length-1] == 'X'){
                wort[wort.Length-1] = 'a';
                wort.Append("X");
                kette += wort + " -> "; 
            }
            else Console.WriteLine("Regel gerade nicht anwendbar.");
        }
        if(eingabe == 2) {
            if(wort[wort.Length-1] == 'X'){
                wort[wort.Length-1] = 'a';
                kette += wort;
            }
            else Console.WriteLine("Regel gerade nicht anwendbar.");
        }
        if(eingabe == 3) {
            if(wort[wort.Length-1] == 'X') {
                wort[wort.Length-1] = 'a';
                wort.Append("Y");
                kette += wort + " -> ";
            }
            else Console.WriteLine("Regel gerade nicht anwendbar.");
        }
        if(eingabe == 4) {
            if(wort[wort.Length-1] == 'Y') {
                wort[wort.Length-1] = 'a';
                wort.Append("a");
                kette += wort;
            }
            else Console.WriteLine("Regel gerade nicht anwendbar.");
        }
        if(eingabe == 5) {
            if(wort[0] == 'S') {
                wort[0] = 'X';
                kette += wort + " -> ";
            }
            else Console.WriteLine("Regel gerade nicht anwendbar.");
        }
        if(eingabe != 0)
            Console.WriteLine(); 
            Console.WriteLine(kette);
            Console.WriteLine(); 
            Console.WriteLine(); 
            
        if(eingabe == 4 || eingabe == 2)
            eingabe = 0;
    }
  }
}